create table themes (
    theme_id char(32) primary key,
    name varchar(255) not null,
    content text not null,
    created_at timestamp not null default now(),
    deleted boolean not null default false
);
