use hyper::{
    body::{Body, HttpBody},
    service::{make_service_fn, service_fn},
    Method, Request, Response, Server, StatusCode,
};
use std::{error::Error, sync::Arc};
use tokio::{sync::RwLock, try_join};
use tokio_postgres::{connect, types::Type, Client, NoTls, Statement};

mod types;

use types::{ErrorResponse, Query, Theme, ThemeId};

struct Database {
    client: Arc<RwLock<Client>>,
    insert_theme: Statement,
    get_theme: Statement,
    invalidate_theme: Statement,
}

async fn connect_to_database() -> Result<Arc<RwLock<Client>>, Box<dyn Error>> {
    let connection_string =
        std::env::var("DB_CONNECTION").expect("no DB_CONNECTION");
    let (client, connection) = connect(&connection_string, NoTls).await?;
    let client = Arc::new(RwLock::new(client));
    let client_on_reconnect = Arc::clone(&client);
    let mut connection = Some(connection);

    tokio::spawn(async move {
        loop {
            if let Some(connection) = connection.take() {
                if let Err(err) = connection.await {
                    dbg!(err);
                }
            } else {
                let (new_client, new_connection) =
                    match connect(&connection_string, NoTls).await {
                        Ok(connection) => connection,
                        Err(err) => {
                            dbg!(err);
                            continue;
                        }
                    };

                connection = Some(new_connection);
                *client_on_reconnect.write().await = new_client;
            }
        }
    });

    Ok(client)
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn Error>> {
    let server_port = std::env::var("SERVER_PORT")
        .ok()
        .and_then(|x| x.parse().ok())
        .expect("no SERVER_PORT");
    let client = connect_to_database().await?;

    let prepare_queries = Arc::clone(&client);
    let prepare_queries = prepare_queries.read().await;
    let insert_theme = prepare_queries.prepare_typed(
        "insert
            into themes (name, content, theme_id)
            values ($1, $2, $3);",
        &[Type::VARCHAR, Type::TEXT, Type::BPCHAR],
    );
    let get_theme = prepare_queries.prepare_typed(
        "select name, content
            from themes
            where
                deleted = false
                and theme_id = $1
                and created_at > now() - interval '1 day';",
        &[Type::BPCHAR],
    );
    let invalidate_theme = prepare_queries.prepare_typed(
        "update themes
            set deleted = true
            where theme_id = $1;",
        &[Type::BPCHAR],
    );

    let (insert_theme, get_theme, invalidate_theme) =
        try_join!(insert_theme, get_theme, invalidate_theme)?;

    let database = Arc::new(Database {
        client,
        insert_theme,
        get_theme,
        invalidate_theme,
    });

    let service = make_service_fn(move |_| {
        let database = Arc::clone(&database);
        async move {
            let service = service_fn(move |request| {
                process_request(Arc::clone(&database), request)
            });
            Ok::<_, hyper::Error>(service)
        }
    });

    let address = ([127, 0, 0, 1], server_port).into();
    let server = Server::bind(&address).serve(service);

    server.await?;

    Ok(())
}

async fn process_query(
    database: Arc<Database>,
    query: Query,
) -> Result<Response<Body>, hyper::Error> {
    let client = database.client.read().await;
    match query {
        Query::UploadTheme(Theme { content, name }) => {
            let sugar = rand::random::<u128>();

            let md5 = md5::compute(format!(
                "{sugar}-{name}-{content}",
                name = &name,
                content = &content,
                sugar = sugar,
            ));
            let theme_id = format!("{:x}", md5);

            let result = client
                .query(&database.insert_theme, &[&name, &content, &theme_id])
                .await;

            if let Err(err) = result {
                dbg!(err);
                internal_error()
            } else {
                theme_id_response(theme_id)
            }
        }
        Query::DownloadTheme(ThemeId { theme_id }) => {
            let result = client.query(&database.get_theme, &[&theme_id]).await;

            let (name, content) = match result {
                Ok(rows) => {
                    if let [row] = rows.as_slice() {
                        (row.get("name"), row.get("content"))
                    } else {
                        return bad_request("no theme with such `themeId`");
                    }
                }
                Err(err) => {
                    dbg!(err);
                    return internal_error();
                }
            };

            let result =
                client.query(&database.invalidate_theme, &[&theme_id]).await;

            if let Err(err) = result {
                dbg!(err);
                internal_error()
            } else {
                theme_response(name, content)
            }
        }
    }
}

async fn process_request(
    database: Arc<Database>,
    request: Request<Body>,
) -> Result<Response<Body>, hyper::Error> {
    let (parts, mut body) = request.into_parts();
    if parts.uri != "/" || parts.method != Method::POST {
        let mut response = Response::new(Body::empty());
        *response.status_mut() = StatusCode::BAD_REQUEST;
        return Ok(response);
    }

    let mut buffer = parts
        .headers
        .get("Content-Length")
        .and_then(|x| x.to_str().ok())
        .and_then(|x| x.parse().ok())
        .map_or_else(Vec::new, Vec::with_capacity);

    while let Some(chunk) = body.data().await {
        let chunk = match chunk {
            Ok(chunk) => chunk,
            Err(..) => return internal_error(),
        };
        buffer.extend(chunk);
    }

    let query = serde_json::from_slice(&buffer);

    match query {
        Ok(query) => process_query(database, query).await,
        Err(error) => bad_request(&error.to_string()),
    }
}

fn bad_request(error: &str) -> Result<Response<Body>, hyper::Error> {
    let error = ErrorResponse { error };
    let body = serde_json::to_vec(&error).unwrap();

    let mut response = Response::new(Body::from(body));
    *response.status_mut() = StatusCode::BAD_REQUEST;

    Ok(response)
}

fn internal_error() -> Result<Response<Body>, hyper::Error> {
    let mut response = Response::new(Body::empty());
    *response.status_mut() = StatusCode::INTERNAL_SERVER_ERROR;

    Ok(response)
}

fn theme_id_response(theme_id: String) -> Result<Response<Body>, hyper::Error> {
    let body = ThemeId { theme_id };
    let body = serde_json::to_vec(&body).unwrap();

    Ok(Response::new(Body::from(body)))
}

fn theme_response(
    name: String,
    content: String,
) -> Result<Response<Body>, hyper::Error> {
    let body = Theme { name, content };
    let body = serde_json::to_vec(&body).unwrap();

    Ok(Response::new(Body::from(body)))
}
