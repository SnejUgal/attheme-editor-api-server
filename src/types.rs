use serde::{Deserialize, Serialize};

#[derive(Deserialize)]
#[serde(rename_all = "camelCase", tag = "requestType", deny_unknown_fields)]
pub enum Query {
    UploadTheme(Theme),
    #[serde(rename_all = "camelCase")]
    DownloadTheme(ThemeId),
}

#[derive(Serialize)]
pub struct ErrorResponse<'a> {
    pub error: &'a str,
}

#[derive(Serialize, Deserialize)]
pub struct Theme {
    pub name: String,
    pub content: String,
}

#[derive(Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct ThemeId {
    pub theme_id: String,
}
